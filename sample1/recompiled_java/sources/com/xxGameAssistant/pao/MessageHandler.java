package com.xxGameAssistant.pao;

import static com.xxGameAssistant.pao.Chinski.STR_CHECK_UPDATE_ERRORS;
import static com.xxGameAssistant.pao.Chinski.STR_NETWORK_ERROR;
import static com.xxGameAssistant.pao.Chinski.STR_NO_UPDATE_AVAILABLE;
import static com.xxGameAssistant.pao.Chinski.STR_TIMEOUT;
import static com.xxGameAssistant.pao.Chinski.STR_UPDATE_DOWNLOAD_ERROR;
import static com.xxGameAssistant.pao.Chinski.STR_UPDATE_NOW;
import static com.xxGameAssistant.pao.WhatCodes.MSG_APK_NOT_FOUND;
import static com.xxGameAssistant.pao.WhatCodes.MSG_INSTALLER_OK;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NET_ERROR;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NEW_VERSION;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NO_NEW_VERSION;
import static com.xxGameAssistant.pao.WhatCodes.MSG_SOCKET_TIMEOUT;
import static com.xxGameAssistant.pao.WhatCodes.MSG_TIPS_ERROR;
import static com.xxGameAssistant.pao.WhatCodes.MSG_TIPS_OK;
import static com.xxGameAssistant.pao.WhatCodes.MSG_VERSION_NOT_FOUND;

import android.os.Handler;
import android.os.Message;

public class MessageHandler extends Handler {


    @Override
    public void handleMessage(Message message) {
        switch (message.what) {
            case MSG_SOCKET_TIMEOUT:
                MainActivity.this.mCheckUpdate.setText(STR_TIMEOUT);
                MainActivity.this.mCheckUpdate.setEnabled(false);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            case MSG_NEW_VERSION:
                MainActivity.this.mCheckUpdate.setText(STR_UPDATE_NOW);
                MainActivity.this.mCheckUpdate.setEnabled(true);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                }
                MainActivity.this.mIsHasNewVersion = true;
                return;
            case MSG_NO_NEW_VERSION:
                MainActivity.this.mCheckUpdate.setText(STR_NO_UPDATE_AVAILABLE);
                MainActivity.this.mCheckUpdate.setEnabled(false);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            case MSG_TIPS_OK:
                MainActivity.this.mAnnouncementText = MTApplication.mTipsText;
                MainActivity.this.mAnnounceTextView.setText(MainActivity.this.mAnnouncementText);
                break;
            case MSG_TIPS_ERROR:
                break;
            case MSG_NET_ERROR:
                MainActivity.this.mCheckUpdate.setText(STR_NETWORK_ERROR);
                MainActivity.this.mCheckUpdate.setEnabled(false);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            case MSG_APK_NOT_FOUND:
                MainActivity.this.mCheckUpdate.setText(STR_UPDATE_DOWNLOAD_ERROR);
                MainActivity.this.mCheckUpdate.setEnabled(false);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            case MSG_VERSION_NOT_FOUND:
                MainActivity.this.mCheckUpdate.setText(STR_CHECK_UPDATE_ERRORS);
                MainActivity.this.mCheckUpdate.setEnabled(false);
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            case MSG_INSTALLER_OK:
                MainActivity.this.mIsInstallOK = true;
                if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                    MainActivity.this.mStartButton.setEnabled(true);
                    return;
                }
                return;
            default:
                super.handleMessage(message);
                return;
        }
        MTApplication.mTipsText = null;
    }
}
