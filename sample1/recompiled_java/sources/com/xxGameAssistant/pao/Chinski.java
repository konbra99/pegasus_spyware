package com.xxGameAssistant.pao;

public class Chinski {
    public static final String STR_TIMEOUT = "连接超时";
    public static final String STR_UPDATE_NOW = "立即更新";
    public static final String STR_NO_UPDATE_AVAILABLE = "暂无更新";
    public static final String STR_NETWORK_ERROR = "网络异常";
    public static final String STR_UPDATE_DOWNLOAD_ERROR = "下载更新错误";
    public static final String STR_CHECK_UPDATE_ERRORS = "检查更新错误";
    public static final String CHECKING_UPDATES = "正在检查更新中";
    public static final String UPDATING_NOW = "正在下载更新";

    /*
    grillowana wieprzowina
            Dziękujemy za skorzystanie z asystenta gry Chacha, podstawowych funkcji wtyczki Daily Cool Running:
                1. Nieograniczony sprint: dostosuj odległość lotu od 400 do 1 miliona metrów, bez rekwizytów i osiągnij prawdziwe niebo!
                2. Podwój nagrodę bonusową: użytkownicy mogą swobodnie wybierać wielokrotność bonusu punktowego i zdobywać dużo punktów!
            Uwaga: ta wtyczka obsługuje obecnie tylko wersję 1.0.6.0 kanału WeChat.
            Mam nadzieję, że ludzie Chacha będą nadal zwracać uwagę na Chacha i przekazywać opinie na temat sugestii i błędów w dowolnym momencie.Rozwój Chacha wymaga pomocy wszystkich w tworzeniu najlepszych wtyczek do gier.
            Sina Weibo: @chacha asystent gry
            Informacje zwrotne o błędzie QQ: 1068071192
            Oficjalna strona internetowa: www.xxzhushou.coms
            Aby korzystać z wtyczki, upewnij się, że przyznałeś uprawnienia administratora
     */
    public static final String CREDITS = """
            各位叉烧
            \t\t感谢使用叉叉游戏助手，天天酷跑插件核心功能：
            \t\t\t1.无限冲刺:自定义4百至100万米飞行距离，免道具，做到真正的一飞冲天！
            \t\t\t2.bonus奖励加倍:用户自由选择得分加成倍数，得分多多！
            \t\t说明：本插件目前只支持微信渠道的1.0.6.0版天天酷跑。
            \t\t希望叉烧们持续关注叉叉，建议、bug随时反馈，叉叉的成长需要各位叉烧的帮助，做最好的游戏插件。
            \t\t新浪微博：@叉叉游戏助手
            \t\tBUG反馈QQ：1068071192
            \t\t官方网站：www.xxzhushou.com\s
            \t\t使用插件一定要授予root权限
            """;
}
