package com.xxGameAssistant.pao;

import static com.xxGameAssistant.pao.Chinski.CHECKING_UPDATES;
import static com.xxGameAssistant.pao.Chinski.STR_CHECK_UPDATE_ERRORS;
import static com.xxGameAssistant.pao.Chinski.STR_NETWORK_ERROR;
import static com.xxGameAssistant.pao.Chinski.STR_NO_UPDATE_AVAILABLE;
import static com.xxGameAssistant.pao.Chinski.STR_TIMEOUT;
import static com.xxGameAssistant.pao.Chinski.STR_UPDATE_DOWNLOAD_ERROR;
import static com.xxGameAssistant.pao.Chinski.STR_UPDATE_NOW;
import static com.xxGameAssistant.pao.Chinski.UPDATING_NOW;
import static com.xxGameAssistant.pao.WhatCodes.MSG_APK_NOT_FOUND;
import static com.xxGameAssistant.pao.WhatCodes.MSG_INSTALLER_OK;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NET_ERROR;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NEW_VERSION;
import static com.xxGameAssistant.pao.WhatCodes.MSG_NO_NEW_VERSION;
import static com.xxGameAssistant.pao.WhatCodes.MSG_SOCKET_TIMEOUT;
import static com.xxGameAssistant.pao.WhatCodes.MSG_TIPS_ERROR;
import static com.xxGameAssistant.pao.WhatCodes.MSG_TIPS_OK;
import static com.xxGameAssistant.pao.WhatCodes.MSG_VERSION_NOT_FOUND;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tendcloud.tenddata.TCAgent;
import com.xxGameAssistant.utility.InjectHelper;
import com.xxGameAssistant.utility.JNIHelper;
import com.xxGameAssistant.utility.PackageHelper;

import org.apache.commons.httpclient.HttpException;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends Activity {
    static int GameInjectionWatcherIntervalInMS = 1;
    static int GameLaunchWatcherIntervalInMS = 1000;
    private TextView mAnnounceTextView = null;
    private String mAnnouncementText = null;
    private EditText mBonus;
    private ArrayList<String> mChannelString = new ArrayList<>();
    private Button mCheckUpdateButton = null;
    private Context mContext;
    private boolean mIsAbortNewVewsion = false;
    private boolean mIsHasMT = false;
    private boolean mIsHasNewVersion = false;
    private boolean mIsInstallOK = false;
    private JNIHelper mJNIHelper;
    private PackageHelper mPackageHelper;
    private ArrayList<String> mPackageString = new ArrayList<>();
    private EditText mRushDistance;
    private TextView mSelectedMT = null;
    private Spinner mSpinner = null;
    private Button mStartButton = null;
    public Handler mHandler = new Handler() {
        /* class com.xxGameAssistant.pao.MainActivity.AnonymousClass2 */

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SOCKET_TIMEOUT:
                    MainActivity.this.mCheckUpdateButton.setText(STR_TIMEOUT);
                    MainActivity.this.mCheckUpdateButton.setEnabled(false);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                case MSG_NEW_VERSION:
                    MainActivity.this.mCheckUpdateButton.setText(STR_UPDATE_NOW);
                    MainActivity.this.mCheckUpdateButton.setEnabled(true);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                    }
                    MainActivity.this.mIsHasNewVersion = true;
                    return;
                case MSG_NO_NEW_VERSION:
                    MainActivity.this.mCheckUpdateButton.setText(STR_NO_UPDATE_AVAILABLE);
                    MainActivity.this.mCheckUpdateButton.setEnabled(false);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                case MSG_TIPS_OK:
                    MainActivity.this.mAnnouncementText = MTApplication.mTipsText;
                    MainActivity.this.mAnnounceTextView.setText(MainActivity.this.mAnnouncementText);
                    break;
                case MSG_TIPS_ERROR:
                    break;
                case MSG_NET_ERROR:
                    MainActivity.this.mCheckUpdateButton.setText(STR_NETWORK_ERROR);
                    MainActivity.this.mCheckUpdateButton.setEnabled(false);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                case MSG_APK_NOT_FOUND:
                    MainActivity.this.mCheckUpdateButton.setText(STR_UPDATE_DOWNLOAD_ERROR);
                    MainActivity.this.mCheckUpdateButton.setEnabled(false);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                case MSG_VERSION_NOT_FOUND:
                    MainActivity.this.mCheckUpdateButton.setText(STR_CHECK_UPDATE_ERRORS);
                    MainActivity.this.mCheckUpdateButton.setEnabled(false);
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                case MSG_INSTALLER_OK:
                    MainActivity.this.mIsInstallOK = true;
                    if (MainActivity.this.mIsHasMT && MainActivity.this.mIsInstallOK && MTApplication.mIsNewestVersion) {
                        MainActivity.this.mStartButton.setEnabled(true);
                        return;
                    }
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
            MTApplication.mTipsText = null;
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doMain();
        this.mContext = this;
        ((MTApplication) getApplication()).setHandler(this.mHandler);
        this.mPackageHelper = new PackageHelper(this);
        TCAgent.initReadManifestTags(this);
        TCAgent.setReportUncaughtExceptions(true);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void doMain() {
        setContentView(R.layout.activity_main);
        this.mIsInstallOK = false;
        this.mSelectedMT = findViewById(R.id.mt_selected_version);
        this.mSpinner = findViewById(R.id.mt_spinner);
        this.mStartButton = findViewById(R.id.startButton);
        this.mStartButton.setEnabled(false);
        this.mCheckUpdateButton = findViewById(R.id.checkUpdate);
        this.mAnnounceTextView = findViewById(R.id.mt_announcement_text);
        this.mAnnounceTextView.setText(MTApplication.mDefTipsText);
        this.mBonus = findViewById(R.id.et_bonus);
        this.mRushDistance = findViewById(R.id.et_rushDistance);

        /* class com.xxGameAssistant.pao.MainActivity.AnonymousClass3 */
        setStartButtonCallback();

        /* class com.xxGameAssistant.pao.MainActivity.AnonymousClass4 */
        setCheckUpdateButtonCallback();

        this.mPackageString = MTApplication.mPackageString;
        this.mChannelString = MTApplication.mChannelString;
        if (!MTApplication.mIsNewestVersion) {
            this.mSelectedMT.setText("插件只支持1.0.7.0版酷跑");
            this.mStartButton.setText("请更新天天酷跑");
            this.mSpinner.setEnabled(false);
            this.mStartButton.setEnabled(false);
        } else if (this.mPackageString == null) {
            this.mSelectedMT.setText("程序发生异常，请重启");
            this.mStartButton.setEnabled(false);
            this.mIsHasMT = false;
            this.mSpinner.setEnabled(false);
        } else if (this.mPackageString.isEmpty()) {
            this.mSelectedMT.setText("未安装天天酷跑");
            this.mStartButton.setEnabled(false);
            this.mIsHasMT = false;
            this.mSpinner.setEnabled(false);
        } else {
            this.mIsHasMT = true;
            this.mSelectedMT.setText("当前选择的游戏:");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.a, this.mChannelString);
            // nigdzie nie moglem znalezc definicji tego 17367049
            adapter.setDropDownViewResource(17367049);
            this.mSpinner.setAdapter(adapter);
        }
    }

    private void setStartButtonCallback() {
        this.mStartButton.setOnClickListener(v -> {
            ((MTApplication) MainActivity.this.getApplication()).setMTPackageName(MainActivity.this.mPackageString.get((int) MainActivity.this.mSpinner.getSelectedItemId()));
            MainActivity.this.mPackageHelper = new PackageHelper(MainActivity.this);
            new MyThread().start();
            synchronized (MainActivity.this.mHandler) {
                MainActivity.this.mIsAbortNewVewsion = true;
                MainActivity.this.mHandler.notify();
            }
            ((MTApplication) MainActivity.this.getApplication()).setHandler(null);
        });
    }

    private void setCheckUpdateButtonCallback() {
        this.mCheckUpdateButton.setOnClickListener(view -> {
            if (!MainActivity.this.mIsHasNewVersion) {
                MainActivity.this.mStartButton.setEnabled(false);
                MainActivity.this.checkUpdate.start();
                MainActivity.this.mCheckUpdateButton.setText(CHECKING_UPDATES);
                MainActivity.this.mCheckUpdateButton.setEnabled(false);
                return;
            }
            MainActivity.this.mStartButton.setEnabled(false);
            MainActivity.this.mCheckUpdateButton.setEnabled(false);
            MainActivity.this.mIsAbortNewVewsion = false;
            MainActivity.this.mCheckUpdateButton.setText(UPDATING_NOW);
            synchronized (MainActivity.this.mHandler) {
                MainActivity.this.mHandler.notify();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        TCAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        TCAgent.onPause(this);
    }

    /* class com.xxGameAssistant.pao.MainActivity.AnonymousClass1 */
    private final Thread checkUpdate = new Thread(() -> {
        try {
            doUpdates();
        } catch (MalformedURLException | InterruptedException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e2) {
            MainActivity.this.mHandler.sendEmptyMessage(MSG_SOCKET_TIMEOUT);
        } catch (IOException e3) {
            e3.printStackTrace();
            MainActivity.this.mHandler.sendEmptyMessage(MSG_NET_ERROR);
        }
    });

    private void doUpdates() throws IOException, PackageManager.NameNotFoundException, InterruptedException {
        byte[] buffer = new byte[4096];
        String strVersion;

        try {
            HttpURLConnection versionConnection = makeHttpConnection(MTApplication.mCheckUpdateURL);
            strVersion = readVersion(versionConnection, buffer);
            versionConnection.disconnect();
        } catch (HttpException e) {
            MainActivity.this.mHandler.sendEmptyMessage(MSG_VERSION_NOT_FOUND);
            return;
        }


        if (isVersionNew(strVersion)) {
            MainActivity.this.mHandler.sendEmptyMessage(MSG_NO_NEW_VERSION);
            return;
        }

        synchronized (MainActivity.this.mHandler) {
            MainActivity.this.mHandler.sendEmptyMessage(MSG_NEW_VERSION);
            MainActivity.this.mHandler.wait();
        }

        if (!MainActivity.this.mIsAbortNewVewsion) {
            File apkFile;

            try {
                HttpURLConnection packageConnection = makeHttpConnection(MTApplication.mPackageURL);
                InputStream in2 = packageConnection.getInputStream();
                apkFile = createApkFile();
                downloadApkFile(in2, apkFile, buffer);
                packageConnection.disconnect();
            } catch (HttpException e) {
                MainActivity.this.mHandler.sendEmptyMessage(MSG_APK_NOT_FOUND);
                return;
            }

            onDownloadedIntent(apkFile);
        }
    }

    private HttpURLConnection makeHttpConnection(final String url) throws IOException {
        HttpURLConnection urlConn = (HttpURLConnection) new URL(url).openConnection();
        urlConn.setDoInput(true);
        urlConn.setUseCaches(false);
        urlConn.setConnectTimeout(MTApplication.mConnectTimeout);
        urlConn.setReadTimeout(MTApplication.mReadTimeout);
        if (urlConn.getResponseCode() != 200) {
            MainActivity.this.mHandler.sendEmptyMessage(MSG_VERSION_NOT_FOUND);
            urlConn.disconnect();
            throw new HttpException();
        }

        return urlConn;
    }

    private String readVersion(final HttpURLConnection connection, final byte[] buffer) throws IOException {
        InputStream in = connection.getInputStream();
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        while (true) {
            int size = in.read(buffer);
            if (size == -1) {
                break;
            }
            bOut.write(buffer, 0, size);
        }

        String version = bOut.toString();
        bOut.close();
        return version;
    }

    private boolean isVersionNew(final String version) throws PackageManager.NameNotFoundException {
        return version.compareTo(MainActivity.this.getPackageManager()
                .getPackageInfo(MainActivity.this.mContext.getPackageName(), 0).versionName) <= 0;
    }

    private File createApkFile() {
        File dir = new File(Environment.getExternalStorageDirectory(), "RhythmPlugin");
        if (dir.exists()) {
            dir.delete();
        }
        dir.mkdirs();
        dir.setExecutable(true);
        dir.setWritable(true);
        dir.setReadable(true);

        File file = new File(dir.getAbsolutePath(), "xxRhythmPlugin_android.apk");
        if (file.exists()) {
            file.delete();
        }
        file.setWritable(true);
        file.setExecutable(true);
        file.setReadable(true);

        return file;
    }

    private void downloadApkFile(final InputStream in2, final File file, final byte[] buffer) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        BufferedOutputStream bufOut = new BufferedOutputStream(out);
        while (true) {
            int size2 = in2.read(buffer);
            if (size2 == -1) {
                bufOut.flush();
                out.flush();
                bufOut.close();
                out.close();
                in2.close();
                return;
            }
            bufOut.write(buffer, 0, size2);
        }
    }

    private void onDownloadedIntent(final File apkFile) {
        Intent intent = new Intent("android.intent.action.VIEW");
        // FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
        MainActivity.this.startActivity(intent);
        MainActivity.this.finish();
    }

    class MyThread extends Thread {
        MyThread() {
        }

        public void run() {
            try {
                String targetPackageName = ((MTApplication) MainActivity.this.getApplication()).getMTPackageName();
                MainActivity.this.mPackageHelper.openApp(targetPackageName);
                while (!MainActivity.this.mPackageHelper.isAppRunning(targetPackageName).booleanValue()) {
                    Thread.sleep(MainActivity.GameInjectionWatcherIntervalInMS);
                }
                Thread.sleep(2000);
                InjectHelper.setGameMode(String.format("%s,%s", MainActivity.this.mBonus.getText(), MainActivity.this.mRushDistance.getText()));
                InjectHelper.executeCmd(MainActivity.this, targetPackageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
