package com.xxGameAssistant.pao;

public class WhatCodes {
    public static final int MSG_SOCKET_TIMEOUT = 0;
    public static final int MSG_NEW_VERSION = 1;
    public static final int MSG_NO_NEW_VERSION = 2;
    public static final int MSG_TIPS_OK = 3;
    public static final int MSG_TIPS_ERROR = 4;
    public static final int MSG_NET_ERROR = 5;
    public static final int MSG_APK_NOT_FOUND = 6;
    public static final int MSG_VERSION_NOT_FOUND = 7;
    public static final int MSG_INSTALLER_OK = 8;
    public static final int MSG_SET_GAMEMODE_OK = 9;
}
