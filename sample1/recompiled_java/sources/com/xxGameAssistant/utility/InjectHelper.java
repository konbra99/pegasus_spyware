package com.xxGameAssistant.utility;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.PrintWriter;

@SuppressLint({"SdCardPath"})
public class InjectHelper {
    static final String targetFuncName = "kick";
    private static String mGameMode = "DEFAULT";

    public static void setGameMode(String gameMode) {
        mGameMode = gameMode;
    }

    public static void executeCmd(Context context, String packageName) {
        try {
            String baseExeDir = NativeFileInstaller.getCacheDir(context);
            String baseSODir = NativeFileInstaller.getSODir(context);
            Process rootProcess = Runtime.getRuntime().exec("su");
            PrintWriter printWriter = new PrintWriter(rootProcess.getOutputStream());
            String cmd = String.format("%s/inject %s %s/libghost.so kick %s",
                    baseExeDir, packageName, baseSODir, mGameMode);
            printWriter.print(cmd);
            printWriter.flush();
            printWriter.close();
            rootProcess.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
