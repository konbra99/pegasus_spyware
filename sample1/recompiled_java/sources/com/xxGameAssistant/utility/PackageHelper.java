package com.xxGameAssistant.utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.tendcloud.tenddata.eActivityTable;

import java.util.List;

public class PackageHelper {
    Activity mActivity;

    public enum PH_FLAG {
        just_invalid,
        act_is_null,
        app_open_succeed,
        app_not_exist
    }

    public PackageHelper(Activity activity) {
        this.mActivity = activity;
    }

    public PH_FLAG openApp(String packageName) {
        PH_FLAG ph_flag = PH_FLAG.just_invalid;
        if (this.mActivity == null) {
            return PH_FLAG.act_is_null;
        }
        PackageManager packageManager = this.mActivity.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            Intent resolveIntent = new Intent("android.intent.action.MAIN", (Uri) null);
            resolveIntent.addCategory("android.intent.category.LAUNCHER");
            resolveIntent.setPackage(packageInfo.packageName);
            ResolveInfo reolveInfo = packageManager.queryIntentActivities(resolveIntent, 0).iterator().next();
            if (reolveInfo == null) {
                return PH_FLAG.app_not_exist;
            }
            String newPackageName = reolveInfo.activityInfo.packageName;
            String className = reolveInfo.activityInfo.name;
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setComponent(new ComponentName(newPackageName, className));
            /* MATCH_DIRECT_BOOT_AUTO
            Querying flag: automatically match components based on their Direct Boot awareness and
            the current user state.

            Since the default behavior is to automatically apply the current user state, this is
            effectively a sentinel value that doesn't change the output of any queries based on its
            presence or absence. */
            intent.setFlags(268435456);
            this.mActivity.startActivity(intent);
            return PH_FLAG.app_open_succeed;
        } catch (PackageManager.NameNotFoundException e) {
            PH_FLAG flag = PH_FLAG.app_not_exist;
            e.printStackTrace();
            return flag;
        }
    }

    public Boolean isAppRunning(String packageName) throws Exception {
        if (this.mActivity == null) {
            throw new Exception("Activity not set, do init first");
        }

        ActivityManager manager = (ActivityManager) this.mActivity.getSystemService(eActivityTable.gACTIVITY);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            for (String s : processInfo.pkgList) {
                if (s.equals(packageName)) {
                    return true;
                }
            }

        }

        return false;
    }
}
